%Load location dataset (CSV), get parsed contents and save to ETS datastore(set) 
%for fast lookup
%Author: Vinay

-module(load_dataset).
-export([start/1]).

-spec(start(string())->string()).

%Reads filename of CSV file to be read (location data) and invokes the CSV parser.
%The location data is stored in ETS for address lookup. 
start(FileName)->
	T1=erlang:now(),

	io:format("Starting\n"),
	
	%Get location data from dataset
	List=erlang:element(2,csv_parser:parse(FileName)),
	io:format("Number of location records: ~p~n",[erlang:length(List)]),
	
	%Table with location data
	Locations=ets:new('locations',[set, named_table]),

	%Table with latitude-longitude as key, & record number
	LatLong=ets:new('latlong',[set, named_table]),

	%Table with state-city as key, & record number
	StateCity=ets:new('statecity',[set, named_table]),

	%Table with zipcode as key, & record number
	Zipcode=ets:new('zipcode',[set, named_table]),
		
	io:format("Storing location records in ETS"),
	lists:foreach(
		fun(X)->
			ets:insert(Locations,erlang:list_to_tuple(X)),
			
			%Insert latitude#longitude or state#city or zipcode as key and other column in table as record number.
			ets:insert(LatLong,erlang:list_to_tuple([lists:nth(7,X)++","++lists:nth(8,X)|[lists:nth(1,X)]])),
			ets:insert(StateCity,erlang:list_to_tuple([lists:nth(5,X)++","++lists:nth(4,X)|[lists:nth(1,X)]])),
			ets:insert(Zipcode,erlang:list_to_tuple([lists:nth(2,X)|[lists:nth(1,X)]])) 
			end,
			List
	),

	%Display time taken to load csv into ets
	T2=erlang:now(),
	Tdiff=["Time taken in microsec:"|[timer:now_diff(T2,T1)]],
	io:format("~p~n",[Tdiff]),

	io:format("Completed\n").
