%Get address corresponding to given inputs - latitude,longitude or state, city or zipcode
%Author: Vinay

-module(address).
-export([fetch/3]).

%Find address for a latitude,longitude as input. Returns complete address along with 
%time taken to fetch result.
fetch(lat_long, Latitude,Longitude)->
	T1=erlang:now(),
	Result=[ets:lookup('locations',ets:lookup_element('latlong',Latitude++","++Longitude,2))],
	
	T2=erlang:now(),
	Tdiff=["Time taken in microsec:"|[timer:now_diff(T2,T1)]],
	[Result|[Tdiff]];

%Find addresss for a state, city as input. Returns complete address along with 
%time taken to fetch result.
fetch(state_city, State, City)->
	T1=erlang:now(),
	Result=[ets:lookup('locations',ets:lookup_element('statecity',State++","++City,2))],	

	T2=erlang:now(),
	Tdiff=["Time taken in microsec:"|[timer:now_diff(T2,T1)]],
	[Result|[Tdiff]];

%Find address for a zipcode as input. Returns complete address along with 
%time taken to fetch result.
fetch(zipcode, Zipcode,"")->
	T1=erlang:now(),
	Result=[ets:lookup('locations',ets:lookup_element('zipcode',Zipcode,2))],

	T2=erlang:now(),
	Tdiff=["Time taken in microsec:"|[timer:now_diff(T2,T1)]],
	[Result|[Tdiff]].	
