# README #

### Location service system in Erlang ###

We are implementing a Location service system in Erlang to get address corresponding to Latitude, Longitude or State, City or Zip Code or Postal Code.

One of the important objectives of the project is to build a low-latency application where the response time to get the address corresponding to given should be less than 20 ms.

### Files ###

csv_parser.erl ->

* Reads csv file with location data into memory and returns this data as tuple.
* Function: parse(FileName)

load_dataset.erl -> 

* Calls csv_parser.erl to get location data and stores it in Erlang table storage (ETS) system for fast query lookup. 
* We have executed this function for 40000 records out of 80000+ records. Run time is 1-2 seconds.
* Function: start(FileName)
	

address.erl->  

* Can be used after location data is loaded into ETS. 
* Takes location input data in the form of Latitude, Longitude or State, City or Zip Code or Postal Code and returns the address after lookup in ETS system if location input is present in ETS, or not found if not present.
* We have executed this function for 40000 records out of 80000+ records. Run time is 13-25 micro seconds for each fetch operation.
* Function: fetch(_input type(atom)_, _parameter1_, _parameter2_)

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Vinay (vinayb211@gmail.com)